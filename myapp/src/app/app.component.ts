import { Component } from '@angular/core';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  nombre: string = 'Alexis Ceballos';
  nombre2: string = 'AlExIs CEbAllOs QuIntAnA';
  arreglo: number[] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  PI: number = Math.PI;
  porcentage: number = 0.234;
  salario: number = 1234.5;
  idioma: string = 'es';
  videoUrl: string = 'https://www.youtube.com/embed/0IxXZPJA3mU';
  personajes: string[] = ['Ironman', 'Spiderema', 'Thor', 'Loki', 'Groot'];
  persona: any = {
    nombre: 'Aexis',
    clave: 'ACqu',
    edad: 500,
    direccion: 'Cr 30A Medellin',
  };
   valorPromesa = new Promise<string>((resolve) => {
    setTimeout(() => {
      resolve('llego la data');
    }, 4500);
  });
  fecha: Date = new Date();
  activar: boolean = true;
}
